import Distribution.Simple.InstallDirs (defaultInstallDirs)


--TP5 Aguero Kevin PROG II
--Funciones Haskell
--i)
potencia :: Integer -> Integer -> Integer
potencia x 0 = 1
potencia x n = x * potencia x (n-1)
--ii)
celciusAFarenheit :: Double -> Double
celciusAFarenheit c = c * 1.8 + 32
--iii)
concatenaLista :: [a] -> [a] -> [a]
concatenaLista [] b = b
concatenaLista (x:xs) b = x : concatenaLista xs b
--iv)
sumarCifra :: Integer -> Integer
sumarCifra x
    |x < 10 = x
    |otherwise = (mod x 10) + sumarCifra(div x 10)
--v)
divisores :: Integral a => a -> [a]
divisores x = [y|y <- [1..x-1], mod x y == 0]

numeroPerfecto :: Integer -> Bool
numeroPerfecto x = x == sum (divisores x)
--vi)
numeroPerfectoMenorA :: Integer -> [Integer]
numeroPerfectoMenorA x = [y | y <- [1..x-1], numeroPerfecto y]
--vii)
sumatoriaV1 :: Integer -> Integer -> Integer
sumatoriaV1 x y
    |x<y = x + sumatoriaV1 (x+1) y
    |x == y = y
    |otherwise = error "Error, el primer argumento tiene que ser menor que el segundo"

sumatoriaV2 :: Integer -> Integer -> Integer 
sumatoriaV2 x y = if x < y
                then x + sumatoriaV2 (x+1) y
                else y
--viii)
maximoDe2 :: Integer -> Integer -> Integer 
maximoDe2 x y
    |x > y = x
    |otherwise = y
--ix)    
maximoDe3 :: Integer -> Integer -> Integer -> Integer
maximoDe3 x y z = maximoDe2 x (maximoDe2 y z)
--x)
listaInvertida :: [a] -> [a]
listaInvertida [] = []
listaInvertida x = last x : listaInvertida (init x)
--xi)
productoPares :: [Integer] -> [Integer] -> [Integer]
productoPares x y = [z*c|z <- y, c <- x,mod z 2 == 0]
--xii)
infixr 3 !&&
(!&&) :: Bool -> Bool -> Bool
True !&& True = False   --infijo
_ !&& _ = True

--xiii)
{--
Si es posible aplicar el concepto de composicion y podriamos utilizarlo para
definir la siguiente funcion
--}

esImpar :: Integer -> Bool
esImpar = not.even

--xiv)
mostrarNotas :: [Double] -> [Double]
mostrarNotas a = a

sumarNotas :: [Double] -> Double
sumarNotas [] = 0
sumarNotas (x:xs) = x + sumarNotas xs

contarNotas :: [Double] -> Double
contarNotas a = sumarNotas [1 | y <- a]

promedio :: Double -> Double -> Double
promedio x y = x / y
--xv)
data JugadorFutbol = Futbolista{nombre::String,edad::Int,posicionesDesempenio::[Integer],cantidadGoles::Integer} deriving Show

jugador1 = Futbolista {
    nombre = "di maria",
    edad = 29,
    posicionesDesempenio = [10],
    cantidadGoles = 57
    }

masDe1Posicion :: JugadorFutbol -> Bool
masDe1Posicion (Futbolista nombre edad posiciones goles)
    | (length posiciones) > 1 = True
    | otherwise = False

agregarPosicion :: Integer -> JugadorFutbol -> [Integer]
agregarPosicion x jugador =  x : posicionesDesempenio jugador

agregarGoles :: Integer -> JugadorFutbol-> Integer
agregarGoles x jugador = x + cantidadGoles jugador

esGoleador :: JugadorFutbol -> Bool
esGoleador jugador = (cantidadGoles jugador) > 20

puedeJugarLigaVeteranos :: JugadorFutbol -> Bool
puedeJugarLigaVeteranos jugador = (edad jugador >= 33) && (edad jugador <= 75)

--xvi

dispersionDe3 :: Integer -> Integer -> Integer -> Integer
dispersionDe3 x y z = maximoDe3 x y z - minimum [x,y,z]

--xvii
tuplaBinaria :: Integer -> Integer -> (Integer, Integer)
tuplaBinaria x y
    |x > y = (x,y)
    |otherwise = (y,x)

--xviii
maximoEntero :: [Integer] -> Integer
maximoEntero [x] = x
maximoEntero (x:y:xs) = if x > y
                        then maximoEntero (x:xs)
                        else maximoEntero (y:xs)

--xix
data Figura = Circulo {radio::Double}
            |Triangulo {c1::Double,c2::Double,c3::Double}
            |Rectangulo {base::Double,altura::Double}
            |Cuadrado {lado::Double}

perimetro :: Figura -> Double
perimetro (Triangulo c1 c2 c3) = c1+c2+c3
perimetro (Circulo radio) = radio * 2 * pi
perimetro (Rectangulo base altura) = base + altura
perimetro (Cuadrado lado) = lado * 4

--xx
data Temp = Centigrado {grados::Double}
            |Farenheit {grados::Double}

centigradosAFahrenheit :: Temp -> Double
centigradosAFahrenheit (Centigrado grados) = celciusAFarenheit grados

fahrenheitACentigrados :: Temp -> Double
fahrenheitACentigrados (Farenheit grados) = (grados - 32) * (5/9)

aguaCongelada :: Temp -> Bool
aguaCongelada (Centigrado grados) = grados == 0
aguaCongelada (Farenheit grados) = grados == 32

--xxi

data Individuo = Hombre {cod1::CuilCuit}
               |Mujer {cod2::CuilCuit}
               |Empresa {cod3::CuilCuit}

data CuilCuit = Codigo {tipo::Integer,numero::Integer,digitoVerificador::Integer}

validarCuitCuil :: Individuo -> Bool

validarCuitCuil (Hombre codigo1)
    |length (listaDigitos (tipo codigo1) ++ listaDigitos (numero codigo1)) == 11
    = verificarTipos (listaDigitos (tipo codigo1)) && verificarDigitos [2,2]
    |otherwise = error "Cantidad de digitos invalido, ingrese un CUIT/CUIL de 11 digitos." 

listaDigitos :: Integer -> [Integer]
listaDigitos x = map (\d -> read [d] :: Integer) (show x)

verificarTipos :: [Integer] -> Bool
verificarTipos x = True

verificarDigitos :: [Integer] -> Bool 
verificarDigitos x = False

let list = listaDigitos (tipo codigo1) ++ listaDigitos (numero codigo1)